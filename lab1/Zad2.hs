equal x y = x == y
smaller x y = x < y

-- greater x y = y < x
greater x y = not (equal x y) && not (smaller x y)

-- smaller_equal x y = not (y < x)
smaller_equal x y = not (greater x y)

-- greater_equal x y = not (x < y)
greater_equal x y = not (smaller x y)

-- not_equal x y = not (x == y)
not_equal x y = not (equal x y)

main = putStrLn (
  "smaller 1 2  == " ++ show (smaller 1 2) ++ "\n" ++
  "greater 2 1 == " ++ show (greater 2 1) ++ "\n" ++
  "equal 1 1 == " ++ show (equal 1 1) ++ "\n" ++
  "smaller_equal 1 1 == " ++ show (smaller_equal 1 1) ++ "\n" ++
  "greater_equal 1 1 == " ++ show (greater_equal 1 1) ++ "\n" ++
  "not_equal 1 0 == " ++ show (not_equal 1 0) ++ "\n"
  )
