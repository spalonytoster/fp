factorial 0 = 1
factorial x = x * factorial (x-1)

main = putStrLn (show (factorial 2))
