same_values fun1 fun2 x y = (fun1 x y) == (fun2 x y)

add x y = x + y
mult x y = x * y

main = putStrLn (
  "same_values add mult 2 2 == " ++ show (same_values add mult 2 2)
  )
