silnia :: Int -> Int
silnia 0 = 1
silnia n = n * silnia (n-1)

silnia2 :: Int -> Int
silnia2 n = silniaInternal n 1
  where
    silniaInternal 0 acc = acc
    silniaInternal n acc = silniaInternal (n-1) (acc * n)

fib :: Int -> Int
fib 0 = 0
fib 1 = 1
fib n = fib (n-2) + fib (n-1)

fib2 :: Int -> Int
fib2 n = fib' 0 1 n
  where
    fib' a b n | n <= 1 = b
               | otherwise = fib' b (a+b) (n-1)

main :: IO ()
main = putStrLn (
  "silnia 5 == " ++ show (silnia 5) ++ " (should be 120)" ++ "\n" ++
  "silnia2 5 == " ++ show (silnia2 5) ++ " (should be 120)" ++ "\n" ++
  "fib 5 == " ++ show (fib 5) ++ " (should be 5)" ++ "\n" ++
  "fib2 5 == " ++ show (fib2 5) ++ " (should be 5)" ++ "\n"
  )
