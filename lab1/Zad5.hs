euclidGcd :: Int -> Int -> Int
-- euclidGcd n m =
--   if mod n m == 0
--     then m
--     else euclidGcd m (mod n m)

euclidGcd n m
  | m == 0 = n
  | otherwise = euclidGcd m (mod n m)

myLcm :: Int -> Int -> Int
myLcm a b = div (a * b) (euclidGcd a b)


main = putStrLn (
  "euclidGcd 468 24 == " ++ show (euclidGcd 468 24) ++ " (should be 12)" ++ "\n" ++
  "euclidGcd 135 19 == " ++ show (euclidGcd 135 19) ++ " (should be 1)" ++ "\n" ++
  "myLcm 21 6 == " ++ show (myLcm 21 6) ++ " (should be 42)" ++ "\n"
  )
