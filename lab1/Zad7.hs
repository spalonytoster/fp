-- a * x^2 + b * x + c
kwad :: (Float, Float, Float) -> (Float, Float)
kwad (a, b, c) = (x1, x2)
  where
    x1 = e + sqrt d / (2 * a)
    x2 = e - sqrt d / (2 * a)
    d = b * b - 4 * a * c
    e = - b / (2 * a)

main :: IO ()
main = putStrLn (
  "kwad 1 0 0 == " ++ show (kwad (1, 0, 0)) ++ " (should be 0)" ++ "\n"
  )
