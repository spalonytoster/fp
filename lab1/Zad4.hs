-- myOdd :: [Int] -> [Bool]
myOdd 1 = True
myOdd n =
  if n > 0 then
    myOdd(n-2)
  else myOdd(n+2)

myEven 0 = True
myEven n = not(myOdd n)

main = putStrLn (
  "odd 5 == " ++ show (myOdd 5) ++ "\n" ++
  "odd -5 == " ++ show (myOdd (-5)) ++ "\n" ++
  "even 5 == " ++ show (myEven 5) ++ "\n" ++
  "even -5 == " ++ show (myEven (-5))
  )
