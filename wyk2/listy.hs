-- append
append [] l = l
append (x:xs) l =
  x:(append xs l)

-- member
-- wersja bardziej imperatywna
member x [] = False
member x (y:ys) =
  if x == y
    then True
  else member x ys

-- wersja funkcyjna
member x (y:ys) =
  (x == y) || (member x ys)


-- square
-- [1, 2, .....4] -- square --> [1, 4, .....9]
f []     = []
f (x:xs) = (square x).(f xs) -- square mozna zamienic na cube ktore jest wbudowane w Haskella (?)

-- map
map f [] = []
map f (x:xs) =
  (f x):(map f xs)

-- > map square [1, 2, 3]
-- > [1, 4, 9]
-- > map length [[1, 2, 3], [1], [1,1]]
-- > [3, 1, 2]

-- o typach:
-- map :: (Int -> Int) -> ([Int] -> [Int])
-- w rzeczywistosci typy nie są okreslane przy samej deklaracji (nie jest sprecyzowane że funkcja f (pierwszy parametr w map) jest Int -> Int)

-- length :: [Int] -> Int
