map2 _ [] [] = []
map2 f [x] [y] = [f x y]
map2 f l1 l2 = map2' [] f l1 l2
  where
    map2' acc _ [] []           = acc
    map2' acc f (s1:l1) []      = map2' (acc++[s1]) f l1 []
    map2' acc f [] (s2:l2)      = map2' (acc++[s2]) f [] l2
    map2' acc f (s1:l1) (s2:l2) = map2' (acc++[f s1 s2]) f l1 l2

pairing :: Num a => [a] -> [String] -> [(a, String)]
pairing [] [] = []
pairing [x] [y] = [(x, y)]
pairing l1 l2 = pairing' [] l1 l2
  where
    pairing' :: Num a => [(a, String)] -> [a] -> [String] -> [(a, String)]
    pairing' acc [] []           = acc
    pairing' acc (s1:l1) []      = pairing' (acc++[(s1, "")]) [] []
    pairing' acc [] (s2:l2)      = pairing' (acc++[(0, s2)]) [] []
    pairing' acc (s1:l1) (s2:l2) = pairing' (acc++[(s1, s2)]) l1 l2

main :: IO ()
main = putStrLn (
 "map2 (+) [1, 2, 3] [4, 5, 6] == " ++ show (map2 (+) [1, 2, 3] [4, 5, 6]) ++
 " (should be [5, 7, 9])" ++ "\n" ++
 "map2 (+) [1, 2, 3] [4, 5, 6, 7] == " ++ show (map2 (+) [1, 2, 3] [4, 5, 6, 7]) ++
 " (should be [5, 7, 9, 7])" ++ "\n" ++
 "map2 (+) [1, 2, 3, 4] [4, 5, 6] == " ++ show (map2 (+) [1, 2, 3, 4] [4, 5, 6]) ++
 " (should be [5, 7, 9, 4])" ++ "\n" ++
 "pairing [1, 2, 3] ['a', 'b', 'c'] == " ++ show (pairing [1, 2, 3] ["a", "b", "c"]) ++
 " (should be [(1,a), (2,b), (3,c)])" ++ "\n" ++
 "\n iloczyn skalarny:" ++ "\n" ++
 "map2 (*) [1, 2, 3] [4, 5, 6] == " ++ show (map2 (*) [1, 2, 3] [4, 5, 6]) ++
 " (should be [4, 10, 18])" ++ "\n"
 )
