filter2 :: Num a => [a] -> (a -> Bool) -> [a]
filter2 [] p = []
filter2 (l1: l) p
  | p l1 = l1:(filter2 l p)
  | otherwise = filter2 l p

main :: IO ()
main = putStrLn (
 "filter2 [1, 2, 3, 4, 5] (>= 3) == " ++ show (filter2 [1, 2, 3, 4, 5] (>= 3)) ++
 " (should be [3, 4, 5])" ++ "\n"
 )
