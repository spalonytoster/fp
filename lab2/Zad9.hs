append :: [a] -> [a] -> [a]
append [] l = l
append (h:t) l =
  h:append t l

-- member :: t -> [t] -> Bool
member x []    = False
member x (h:t) =
  x == h || member x t

reverseList :: [t] -> [t]
reverseList []    = []
reverseList (h:t) =
  reverseList t ++ [h]

lastElem :: [Int] -> Int
lastElem []     = -1
lastElem [h]    = h
lastElem (l1:l) = lastElem l

-- deleteElem x [] = []
deleteElem x [] = []
deleteElem x (l1:l)
  | x == l1 = l
  | otherwise = deleteElem x (l ++ [l1])

splitList :: Ord a => a -> [a] -> ([a], [a])
splitList x l = splitList' x [] [] l
    where
      splitList' x a b [] = (a, b)
      splitList' x a b (l1:l)
        | l1 < x    = splitList' x (l1:a)  b      l
        | l1 > x    = splitList' x a       (l1:b) l
        | otherwise = splitList' x a       b      l

main :: IO ()
main = putStrLn (
  "append [1,2] [4,5] == " ++ show (append [1,2] [4,5]) ++ " (should be [1,2,4,5])" ++ "\n" ++
  "\n" ++
  "member [1,2,3] 2 == " ++ show (member 2 [1,2,3]) ++ " (should be True)" ++ "\n" ++
  "member [1,2,3] 4 == " ++ show (member 4 [1,2,3]) ++ " (should be False)" ++ "\n" ++
  "\n" ++
  "reverseList [1,2,3] == " ++ show (reverseList [1, 2 ,3]) ++ " (should be [3,2,1])" ++ "\n" ++
  "reverseList ['a', 'b', 'c'] == " ++ show (reverseList ['a', 'b', 'c']) ++ " (should be \"cba\")" ++ "\n" ++
  "\n" ++
  "lastElem [1,2,3] == " ++ show (lastElem [1, 2 ,3]) ++ " (should be 3)" ++ "\n" ++
  "\n" ++
  "deleteElem 3 [1,2,3,4,5] == " ++ show (deleteElem 3 [1, 2, 3, 4, 5]) ++ " (should be [4,5,1,2])" ++ "\n" ++
  "\n" ++
  "splitList 3 [1,2,3,4,5] == " ++ show (splitList 3 [1,2,3,4,5]) ++ " (should be ([2,1], [5,4])" ++ "\n" ++
  "\n"
  )
