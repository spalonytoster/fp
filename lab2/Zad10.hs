insertionSort :: (a -> a -> Bool) -> [a] -> [a]
insertionSort _ [] = []
insertionSort _ [l1] = [l1]
insertionSort comp (l1:l) = insert l1 (insertionSort comp l)
  where
    insert x [] = [x]
    insert x (l1:l)
      | comp x l1 = x:l1:l
      | otherwise = l1:(insert x l)


main :: IO ()
main = putStrLn (
 "insertionSort [1, 5, 2, 4, 3] == " ++ show (insertionSort (<) [1, 5, 2, 4, 3]) ++
 " (should be [1, 2, 3, 4, 5])" ++ "\n" ++
 "insertionSort [1, 5, 2, 4, 3] == " ++ show (insertionSort (>) [1, 5, 2, 4, 3]) ++
 " (should be [5, 4, 3, 2, 1])" ++ "\n"
 )
