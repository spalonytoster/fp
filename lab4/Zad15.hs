insertionSort :: (a -> a -> Bool) -> [a] -> [a]
insertionSort comp (l1:l) = foldl (insert) [l1] l
  where
    insert [] next = [next]
    insert (acc1:acc) next
      | comp next acc1 = next:acc1:acc
      | otherwise = acc1:(insert acc next)

main :: IO ()
main = putStrLn (
 "insertionSort [1, 5, 2, 4, 3] == " ++ show (insertionSort (<) [1, 5, 2, 4, 3]) ++
 " (should be [1, 2, 3, 4, 5])" ++ "\n"
 )
