foldl2 :: (b -> a -> b) -> b -> [a] -> b
foldl2 f acc []     = acc
foldl2 f acc (x:xs) = foldl2 f (f acc x) xs

foldr2 :: (b -> a -> b) -> b -> [a] -> b
foldr2 f acc (x:xs) = foldl2 f acc (reverse (x:xs))

foldr3 f acc []     = acc
foldr3 f acc (x:xs) = f x (foldr3 f acc xs)

prod 1 = 1
prod n = foldl2 (*) 1 [1..n]

length2 []     = 0
length2 (x:xs) = 1 + (length2 xs)

and2 :: [Bool] -> Bool
and2 [b]    = b
and2 (l1:l) = (&&) l1 (and2 l)

-- nwd ::
nwd (l1:l) = foldl2 (gcd) l1 l
-- nwd l      = foldl2 (gcd) 0 l

delete :: Eq p => [p] -> p -> [p]
delete l x = foldl2 equal [] l
  where
    equal acc next
      | next == x = acc
      | otherwise = acc ++ [next]

map2 l f = foldl2 mapElem [] l
  where
    mapElem acc next = acc ++ [f next]

reverse2 l = foldl2 reverse' [] l
  where
    reverse' acc next = next:acc

filter2 p l = foldl2 filter' [] l
  where
    filter' acc next
      | p next = acc ++ [next]
      | otherwise = acc

forall p l = foldl2 check True l
  where
    check acc next = acc && p next


main :: IO ()
main = putStrLn (
  "foldl2 (+) 0 [1,2,3] == " ++ show (foldl2 (+) 0 [1,2,3]) ++ " (should be 6)" ++ "\n" ++
  "\n" ++
  "foldr2 (-) 6 [1,2,3] == " ++ show (foldr2 (-) 6 [1,2,3]) ++ " (should be 0)" ++ "\n" ++
  "\n" ++
  "foldr3 (-) 0 [1,2,3] == " ++ show (foldr3 (-) 0 [1,2,3]) ++ " (should be -6)" ++ "\n" ++
  "\n" ++
  "prod 4 == " ++ show (prod 4) ++ " (should be 24)" ++ "\n" ++
  "\n" ++
  "length2 [1,2,3] == " ++ show (length2 [1,2,3]) ++ " (should be 3)" ++ "\n" ++
  "\n" ++
  "and2 [True, True, True] == " ++ show (and2 [True, True, True]) ++ " (should be True)" ++ "\n" ++
  "\n" ++
  "and2 [True, True, False] == " ++ show (and2 [True, True, False]) ++ " (should be False)" ++ "\n" ++
  "\n" ++
  "nwd [24, 16, 12] == " ++ show (nwd [24, 16, 12]) ++ " (should be 4)" ++ "\n" ++
  "\n" ++
  "delete [1,2,3,4,5] 3 == " ++ show (delete [1,2,3,4,5] 3) ++ " (should be [1,2,4,5])" ++ "\n" ++
  "\n" ++
  "map2 [1,2,3,4,5] (* 2) == " ++ show (map2 [1,2,3,4,5] (* 2)) ++ " (should be [2,4,6,8,10])" ++ "\n" ++
  "\n" ++
  "reverse2 [1,2,3,4,5] == " ++ show (reverse2 [1,2,3,4,5]) ++ " (should be [5,4,3,2,1])" ++ "\n" ++
  "\n" ++
  "filter2 (>= 3) [1,2,3,4,5] == " ++ show (filter2 (>= 3) [1,2,3,4,5]) ++ " (should be [3,4,5])" ++ "\n" ++
  "\n" ++
  "forall (\\x -> (mod x 2) == 0) [1,2,3,4,5] == " ++ show (forall (\x -> (mod x 2) == 0) [1,2,3,4,5]) ++
  " (should be False)" ++ "\n" ++
  "\n" ++
  "forall (\\x -> (mod x 2) == 0) [2,4,6,8,10] == " ++ show (forall (\x -> (mod x 2) == 0) [2,4,6,8,10]) ++
  " (should be True)" ++ "\n" ++
  "\n"
  )
